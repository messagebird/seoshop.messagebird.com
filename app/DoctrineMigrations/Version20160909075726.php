<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160909075726 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE bulk_messages (id VARCHAR(36) NOT NULL, seoshop_shop VARCHAR(36) DEFAULT NULL, order_from_date DATETIME NOT NULL, order_to_date DATETIME NOT NULL, article_code VARCHAR(255) DEFAULT NULL, message LONGTEXT NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME NOT NULL, INDEX IDX_BD306B53A2FF4E9B (seoshop_shop), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messages (id VARCHAR(36) NOT NULL, seoshop_action VARCHAR(36) DEFAULT NULL, bulk_message VARCHAR(36) DEFAULT NULL, client_data LONGTEXT NOT NULL, phone_number VARCHAR(30) NOT NULL, message TINYTEXT NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME NOT NULL, INDEX IDX_DB021E96A7466E91 (seoshop_action), INDEX IDX_DB021E9682F33FA5 (bulk_message), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messagebird_account (id VARCHAR(36) NOT NULL, seoshop_shop VARCHAR(36) DEFAULT NULL, api_key VARCHAR(255) DEFAULT NULL, sender VARCHAR(255) NOT NULL, contact_name VARCHAR(255) NOT NULL, contact_number VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_35889DFFA2FF4E9B (seoshop_shop), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE seoshop_action (id VARCHAR(36) NOT NULL, seoshop_shop VARCHAR(36) DEFAULT NULL, seoshop_hook VARCHAR(36) DEFAULT NULL, action VARCHAR(255) NOT NULL, to_owner TINYINT(1) NOT NULL, to_client TINYINT(1) NOT NULL, message LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, modified_at DATETIME NOT NULL, INDEX IDX_A7466E91A2FF4E9B (seoshop_shop), INDEX IDX_A7466E91AACD416C (seoshop_hook), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE seoshop_hook (id VARCHAR(36) NOT NULL, seoshop_shop VARCHAR(36) DEFAULT NULL, hook_id VARCHAR(45) DEFAULT NULL, item_group VARCHAR(255) NOT NULL, item_action VARCHAR(255) NOT NULL, enabled TINYINT(1) DEFAULT \'0\' NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME NOT NULL, INDEX IDX_AACD416CA2FF4E9B (seoshop_shop), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE seoshop_shop (id VARCHAR(36) NOT NULL, seoshop_id VARCHAR(36) NOT NULL, seoshop_api_key VARCHAR(255) NOT NULL, seoshop_api_token VARCHAR(255) NOT NULL, seoshop_language VARCHAR(255) NOT NULL, seoshop_name VARCHAR(255) DEFAULT NULL, enabled TINYINT(1) DEFAULT \'1\' NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bulk_messages ADD CONSTRAINT FK_BD306B53A2FF4E9B FOREIGN KEY (seoshop_shop) REFERENCES seoshop_shop (id)');
        $this->addSql('ALTER TABLE messages ADD CONSTRAINT FK_DB021E96A7466E91 FOREIGN KEY (seoshop_action) REFERENCES seoshop_action (id)');
        $this->addSql('ALTER TABLE messages ADD CONSTRAINT FK_DB021E9682F33FA5 FOREIGN KEY (bulk_message) REFERENCES bulk_messages (id)');
        $this->addSql('ALTER TABLE messagebird_account ADD CONSTRAINT FK_35889DFFA2FF4E9B FOREIGN KEY (seoshop_shop) REFERENCES seoshop_shop (id)');
        $this->addSql('ALTER TABLE seoshop_action ADD CONSTRAINT FK_A7466E91A2FF4E9B FOREIGN KEY (seoshop_shop) REFERENCES seoshop_shop (id)');
        $this->addSql('ALTER TABLE seoshop_action ADD CONSTRAINT FK_A7466E91AACD416C FOREIGN KEY (seoshop_hook) REFERENCES seoshop_hook (id)');
        $this->addSql('ALTER TABLE seoshop_hook ADD CONSTRAINT FK_AACD416CA2FF4E9B FOREIGN KEY (seoshop_shop) REFERENCES seoshop_shop (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE messages DROP FOREIGN KEY FK_DB021E9682F33FA5');
        $this->addSql('ALTER TABLE messages DROP FOREIGN KEY FK_DB021E96A7466E91');
        $this->addSql('ALTER TABLE seoshop_action DROP FOREIGN KEY FK_A7466E91AACD416C');
        $this->addSql('ALTER TABLE bulk_messages DROP FOREIGN KEY FK_BD306B53A2FF4E9B');
        $this->addSql('ALTER TABLE messagebird_account DROP FOREIGN KEY FK_35889DFFA2FF4E9B');
        $this->addSql('ALTER TABLE seoshop_action DROP FOREIGN KEY FK_A7466E91A2FF4E9B');
        $this->addSql('ALTER TABLE seoshop_hook DROP FOREIGN KEY FK_AACD416CA2FF4E9B');
        $this->addSql('DROP TABLE bulk_messages');
        $this->addSql('DROP TABLE messages');
        $this->addSql('DROP TABLE messagebird_account');
        $this->addSql('DROP TABLE seoshop_action');
        $this->addSql('DROP TABLE seoshop_hook');
        $this->addSql('DROP TABLE seoshop_shop');
    }
}
