<?php
/**
 * Created by Notive.
 * User: Ferry Koster
 * Date: 15:02 21-03-15
 */

namespace Notive\SEOshopBundle\Model;


interface WebshopInterface
{
    /**
     * Get the SEOshop's id
     *
     * @return integer
     */
    public function getSeoshopId();

    /**
     * Get the SEOshop's API key
     *
     * @return string
     */
    public function getSeoshopApiKey();

    /**
     * Get the SEOshop's API token
     *
     * @return string
     */
    public function getSeoshopApiToken();

    /**
     * Get the SEOshop's language
     *
     * @return string
     */
    public function getSeoshopLanguage();

    /**
     * True if the SEOshop is active
     *
     * @return boolean
     */
    public function isActive();

    /**
     * @param string $seoshopApiKey
     * @return self
     */
    public function setSeoshopApiKey($seoshopApiKey);

    /**
     * @param string $seoshopApiToken
     * @return self
     */
    public function setSeoshopApiToken($seoshopApiToken);

    /**
     * @param string $seoshopId
     * @return self
     */
    public function setSeoshopId($seoshopId);

    /**
     * @param string $seoshopLanguage
     * @return self
     */
    public function setSeoshopLanguage($seoshopLanguage);

    /**
     * @param string $isActive
     * @return self
     */
    public function setIsActive($isActive);


}