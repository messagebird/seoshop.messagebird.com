<?php
/**
 * Created by Notive.
 * User: Ferry Koster
 * Date: 14:59 21-03-15
 */

namespace Notive\SEOshopBundle\Model;

use Doctrine\ORM\Mapping as ORM;

class Webshop implements WebshopInterface
{
    protected $id;

    /**
     * @ORM\Column(type="integer", name="seoshop_id")
     * @var integer
     */
    protected $seoshopId;

    /**
     * @ORM\Column(type="string", length=255, name="seoshop_api_key")
     * @var integer
     */
    protected $seoshopApiKey;

    /**
     * @ORM\Column(type="string", length=255, name="seoshop_api_token")
     * @var integer
     */
    protected $seoshopApiToken;

    /**
     * @ORM\Column(type="string", length=255, name="seoshop_language")
     * @var integer
     */
    protected $seoshopLanguage;

    /**
     * @ORM\Column(type="boolean", name="is_active", options={"default"=false})
     * @var boolean
     */
    protected $isActive = false;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $seoshopApiKey
     * @return self
     */
    public function setSeoshopApiKey($seoshopApiKey)
    {
        $this->seoshopApiKey = $seoshopApiKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSeoshopApiKey()
    {
        return $this->seoshopApiKey;
    }

    /**
     * @param mixed $seoshopApiToken
     * @return self
     */
    public function setSeoshopApiToken($seoshopApiToken)
    {
        $this->seoshopApiToken = $seoshopApiToken;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSeoshopApiToken()
    {
        return $this->seoshopApiToken;
    }

    /**
     * @param mixed $seoshopId
     * @return self
     */
    public function setSeoshopId($seoshopId)
    {
        $this->seoshopId = $seoshopId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSeoshopId()
    {
        return $this->seoshopId;
    }

    /**
     * @param mixed $seoshopLanguage
     * @return self
     */
    public function setSeoshopLanguage($seoshopLanguage)
    {
        $this->seoshopLanguage = $seoshopLanguage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSeoshopLanguage()
    {
        return $this->seoshopLanguage;
    }

    /**
     * @param boolean $isActive
     * @return self
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->isActive;
    }

    public function createApiClient()
    {
        return new \WebshopappApiClient(
            'live',
            $this->getSeoshopApiKey(),
            $this->getSeoshopApiToken(),
            $this->getSeoshopLanguage()
        );
    }

}