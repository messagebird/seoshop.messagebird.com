<?php
/**
 * Created by Notive.
 * User: Ferry Koster
 * Date: 16:30 21-03-15
 */

namespace Notive\SEOshopBundle\Helper;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Notive\SEOshopBundle\Model\WebshopInterface;

class WebshopManager
{
    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var string
     */
    protected $class;

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * @param string        $apiKey
     * @param string        $apiSecret
     * @param EntityManager $em
     * @param string        $class
     */
    public function __construct($apiKey, $apiSecret, $em, $class)
    {
        $this->key = $apiKey;
        $this->secret = $apiSecret;

        $this->entityManager = $em;
        $this->class = $class;
        $this->repository = $this->entityManager->getRepository('AppBundle:SeoshopShop');
    }


    /**
     * @return WebshopInterface
     */
    public function createWebshop()
    {
        $class = $this->class;
        $webshop = new $class;
        $webshop->setSeoshopApiKey($this->key);
        return $webshop;
    }

    public function persist(WebshopInterface $webshop)
    {
        $this->entityManager->persist($webshop);
        $this->entityManager->flush();
    }

    /**
     * @return EntityRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param string $token
     * @return string
     */
    public function generateApiToken($token)
    {
        return md5($token . $this->secret);
    }

    /**
     * @param string $language
     * @param string $shopId
     * @param string $timestamp
     * @param string $token
     * @param string $installSignature
     * @return bool
     */
    public function validateInstallSignature($language, $shopId, $timestamp, $token, $installSignature)
    {
        $params = array(
            'language' => $language,
            'shop_id' => $shopId,
            'timestamp' => $timestamp,
            'token' => $token
        );
        ksort($params);

        $signature = '';
        foreach ($params as $key => $value) {
            $signature .= $key.'='.$value;
        }

        return $installSignature == md5($signature . $this->secret);
    }

    /**
     * @param string $requestSignature
     * @param string $requestPayload
     * @return bool
     */
    public function validateRequestSignature($requestSignature = null, $requestPayload = null)
    {
        return !(is_null($requestSignature) || $requestSignature != md5($requestPayload . $this->secret));
    }

} 