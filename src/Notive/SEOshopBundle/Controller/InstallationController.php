<?php
/**
 * Created by Notive.
 * User: Ferry Koster
 * Date: 16:01 21-03-15
 */

namespace Notive\SEOshopBundle\Controller;


use AppBundle\Entity\SeoshopShop;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Notive\SEOshopBundle\Helper\WebshopManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class InstallationController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function successAction(Request $request)
    {
        /**
         * @var $manager WebshopManager
         * @var $repository EntityRepository
         */
        $manager = $this->get('seoshop.webshop_manager');
        $repository = $manager->getRepository();

        $language =     $request->get('language');
        $shopId =       $request->get('shop_id');
        $timestamp =    $request->get('timestamp');
        $token =        $request->get('token');
        $signature =    $request->get('signature');

        $isValid = $manager->validateInstallSignature($language, $shopId, $timestamp, $token, $signature);

        if ($isValid) {
            $webshop = $repository->findOneBy(['seoshopId' => $shopId]);
            if (!$webshop) {
                $webshop = $manager->createWebshop();
                $webshop->setSeoshopId($shopId);
            }
            $webshop->setSeoshopLanguage($language);
            $webshop->setSeoshopApiToken($manager->generateApiToken($token));
            $webshop->setIsActive(1);
            $webshop->onCreate();
            $manager->persist($webshop);

            return $this->redirect($this->generateUrl( $this->container->getParameter('seoshop.success_route'), [
                'seoshopId' => $webshop->getSeoshopId(),
                'seoshopApiToken' => $webshop->getSeoshopApiToken()
            ]));
        }

        return $this->render('SEOshopBundle:Installation:error.html.twig', [
            'app_name' => $this->container->getParameter('seoshop.app_name')
        ], new Response('', 500));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function cancelledAction(Request $request)
    {
        // TODO: Do something
        return $this->render('SEOshopBundle:Installation:cancelled.html.twig', [
            'app_name' => $this->container->getParameter('seoshop.app_name')
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function uninstallAction(Request $request)
    {
        // TODO: Do something
        return $this->render('SEOshopBundle:Installation:uninstall.html.twig', [
            'app_name' => $this->container->getParameter('seoshop.app_name')
        ]);
    }
} 