<?php

namespace Notive\SEOshopBundle;

use Notive\SEOshopBundle\DependencyInjection\SEOshopExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SEOshopBundle extends Bundle
{
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new SEOshopExtension();
        }

        return $this->extension;
    }
}
