<?php

namespace AppBundle\Controller;

use Notive\SEOshopBundle\Helper\WebshopManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Model\Message;
use AppBundle\Entity\SeoshopShop;
use AppBundle\Entity\SeoshopHook;
use AppBundle\Entity\MessagebirdAccount;

/**
 * Class MessageBirdController
 * @package AppBundle\Controller
 */
class MessageBirdController extends Controller
{
    /**
     * @param Request $request
     * @param $seoshopId
     * @param $webhookId
     * @return Response
     * @throws \Exception
     */
    public function orderHookAction(Request $request, $seoshopId, $webhookId)
    {
        $logger = $this->get('logger');
        $payload = json_decode($this->validateHeaderSignature(), true);
        $xEvent = $request->headers->get("X-Event");
        $service = $this->get('messagebird.webservice');
        $em = $this->getDoctrine()->getManager();
        $shopRepo = $em->getRepository('AppBundle:SeoshopShop');
        $accountRepo = $em->getRepository('AppBundle:MessagebirdAccount');
        $hookRepo = $em->getRepository('AppBundle:SeoshopHook');

        $shop = $shopRepo->find($seoshopId);
        if (!$shop) {
            $logger->addAlert('shop ' . $seoshopId . ' not found');
            return new Response('shop ' . $seoshopId . ' not found', 404);
        }
        if (!$shop->isActive()) {
            return new Response('inactive', 200);
        }
        $account = $accountRepo->findOneBy(array('seoshopShop' => $shop,));
        if (!$account) {
            $logger->addAlert('account for shop ' . $seoshopId . ' not found');
            return new Response('account for shop ' . $seoshopId . ' not found', 404);
        }
        $hook = $hookRepo->find($webhookId);
        if (!$hook) {
            $logger->addAlert('webhook ' . $webhookId . ' not found');
            return new Response('webhook ' . $webhookId . ' not found', 404);
        }
        if (!$hook->isEnabled()) {
            return new Response('inactive', 200);
        }

        if (!isset($payload['order'])) {
            $logger->addAlert('Bad request made to order hook');
            return new Response('Bad Request', 400);
        }
        $order = $payload['order'];
        $country = $order['addressBillingCountry'];

        $number = $order['mobile'];
        if (empty($number)) {
            $number = $order['phone'];
        }

        $message = new Message();
        $message->setFirstName($order['firstname']);
        $message->setLastName($order['lastname']);
        $message->setCountryCode($country['code']);
        $message->setRawPhoneNumber($number);
        $message->setPhoneNumber($number);
        $message->setOrderNumber($order['number']);
        $message->setSender($account->getSender());
        $message->setSenderName($account->getContactName());
        $message->setRawSenderNumber($account->getContactNumber());
        $message->setSenderNumber($account->getContactNumber());
        $message->setSenderCountry($account->getCountry());
        $message->setOrderStatus($order['status']);
        if ($order['shipmentStatus'] == 'shipped') {
            $message->setTntCode($order['shipments']['resource']['embedded'][0]['trackingCode']);
        }

        $result = $service->sendOrderMessage($message, $shop, $hook, $account->getApiKey());
        if (!$result) {
            $logger->addCritical('MessageBird: ERROR during sending process for shop ' . $shop->getId() . ' hook ' . $hook->getId());
        }

        return new Response("Handled", 200);
    }

    /**
     * @param Request $request
     * @param $seoshopId
     * @param $webhookId
     * @return Response
     * @throws \Exception
     */
    public function productHookAction(Request $request, $seoshopId, $webhookId)
    {
        $logger = $this->get('logger');
        $payload = json_decode($this->validateHeaderSignature(), true);
        $xEvent = $request->headers->get("X-Event");
        $service = $this->get('messagebird.webservice');
        $em = $this->getDoctrine()->getManager();
        $shopRepo = $em->getRepository('AppBundle:SeoshopShop');
        $accountRepo = $em->getRepository('AppBundle:MessagebirdAccount');
        $hookRepo = $em->getRepository('AppBundle:SeoshopHook');

        $shop = $shopRepo->find($seoshopId);
        if (!$shop) {
            $logger->addAlert('shop ' . $seoshopId . ' not found');
            return new Response('shop ' . $seoshopId . ' not found', 404);
        }
        if (!$shop->isActive()) {
            return new Response('inactive', 200);
        }
        $account = $accountRepo->findOneBy(array('seoshopShop' => $shop,));
        if (!$account) {
            $logger->addAlert('account for shop ' . $seoshopId . ' not found');
            return new Response('account for shop ' . $seoshopId . ' not found', 404);
        }
        $hook = $hookRepo->find($webhookId);
        if (!$hook) {
            $logger->addAlert('webhook ' . $webhookId . ' not found');
            return new Response('webhook ' . $webhookId . ' not found', 404);
        }
        if (!$hook->isEnabled()) {
            return new Response('inactive', 200);
        }

        $logger->addCritical('HEY LOOK BELOW');
        $logger->addDebug(serialize($payload));
        $logger->addCritical('HEY LOOK ABOVE');

        if (!isset($payload['product'])) {
            $logger->addAlert('Bad request made to product hook');
            return new Response('Bad Request', 400);
        }
        $product = $payload['product'];

        $message = new Message();
        $message->setProduct($product['id']);
        $message->setSender($account->getSender());
        $message->setSenderName($account->getContactName());
        $message->setSenderNumber($account->getContactNumber());
        $message->setSenderCountry($account->getCountry());

        $result = $service->sendProductMessage($message, $shop, $hook, $account->getApiKey());

        if (!$result) {
            $logger->addCritical('MessageBird: ERROR during sending process for shop ' . $shop->getId() . ' hook ' . $hook->getId());
        }

        return new Response("Handled", 200);
    }

    public function healthCheckAction(Request $request)
    {
        return new Response("", 200);
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function validateHeaderSignature()
    {
        if (!function_exists('apache_request_headers')) {
            function apache_request_headers()
            {
                foreach ($_SERVER as $key => $value) {
                    if (substr($key, 0, 5) == "HTTP_") {
                        $key = str_replace(" ", "-", ucwords(strtolower(str_replace("_", " ", substr($key, 5)))));
                        $out[$key] = $value;
                    } else {
                        $out[$key] = $value;
                    }
                }
                return $out;
            }
        }

        /** @var WebshopManager $wsHelper */
        $wsHelper = $this->get('seoshop.webshop_manager');
        $input = file_get_contents('php://input');
        $payload = !empty($input) ? $input : '';
        $headers = apache_request_headers();
        $signature = isset($headers['X-Signature']) ? $headers['X-Signature'] : null;

        if (isset($headers['X-Skip'])) {
            return $payload;
        }

        if ($wsHelper->validateRequestSignature($signature, $payload) == false) {
            throw new \Exception('Could not validate signature');
        }
        return $payload;
    }
}
