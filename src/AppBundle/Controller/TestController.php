<?php
namespace AppBundle\Controller;

use AppBundle\Entity\SeoshopAction;
use AppBundle\Entity\SeoshopShop;
use AppBundle\Form\ActionsCollectionType;
use AppBundle\Form\ActionsType;
use AppBundle\Form\SeoshopActionType;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TestController extends Controller
{
    /**
     * @Route("/seoshop/ajax")
     * @param Request $request
     */
    public function indexAction(Request $request)
    {
        $this->outputJson($request->request);
    }

    /**
     * @Route("/collections", name="homepage")
     * @param none
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function collectionsAction(Request $request)
    {
        // Display a ActionCollectionType form and properly render all fields
        // Now disable the submit feature and get it working with ajax calls
        // Now you can set a action url to post your formdata
        // Formdata should be taken for validation and returns a message if the action was successfull or not.


        $session = $this->get('session');
        $shopId = $session->get("seoshop_id");
        $em = $this->getDoctrine()->getManager();
        $seoshop = $em->getRepository('AppBundle:SeoshopShop')
            ->findOneBy(array(
                'id' => $shopId
        ));

        $actions = $this
            ->getDoctrine()
            ->getRepository('AppBundle:SeoshopAction')
            ->findBy(array(
                'seoshopShop' => $shopId
        ));

        foreach ($actions as $action) {
            $seoshop->setActions($action);
        }

        $actionForm = $this->createForm(ActionsType::class, $seoshop);
        $actionForm->handleRequest($request);

        if($actionForm->isValid() && $actionForm->isSubmitted()){
            dump($actionForm->getData());
        }

        return $this->render('AppBundle::error.html.twig', array(
            'form' => $actionForm->createView()
        ));
    }

    public function bulkAction()
    {
        return $this->render('');
    }

    /**
     * @Route("/testsettings")
     * @param Request $request
     */
    public function settingsAction(Request $request)
    {
        header('Content-Type: application/json');
        $id = $request->get('id');

        if ($id){
            // Got id, find our entity here
            $settingsAction = $this->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:SeoshopAction')
                ->findOneBy(array(
                    'id' => $id
            ));

            if(!$settingsAction){
                // Could not find entity
                $settingsAction = new SeoshopAction();
            }

            die(
                json_encode(array(
                    $settingsAction
                ))
            );
        }
    }
}
