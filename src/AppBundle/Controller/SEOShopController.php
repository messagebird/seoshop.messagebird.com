<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BulkMessage;
use AppBundle\Entity\Message;
use AppBundle\Entity\MessagebirdAccount;
use AppBundle\Model\Message as MessageModel;
use AppBundle\Entity\SeoshopAction;
use AppBundle\Entity\SeoshopShop;
use AppBundle\Form\BulkMessageType;
use AppBundle\Form\MessagebirdAccountType;
use AppBundle\Form\SeoshopActionType;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\FOSRestController;
use MessageBird\Client;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Notive\SEOshopBundle\Helper\WebshopManager;
use Doctrine\ORM\EntityRepository;
use AppBundle\Services\SeoshopService;

/**
 * Class SEOShopController
 * @package AppBundle\Controller
 */
class SEOShopController extends FOSRestController
{

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function installAction(Request $request)
    {
        if ($request->get('seoshopApiToken')) {
            return $this->render(
                'AppBundle::welcome.html.twig',
                [
                    'app_name' => $this->container->getParameter('seoshop.app_name')
                ],
                new Response('', 200)
            );
        }

        $shopId = $request->get('shop_id');

        $shop = $this->get('doctrine')->getManager()->getRepository('AppBundle:SeoshopShop')->findOneBy(
            [
                'seoshopId' => $shopId,
                'enabled' => true
            ]
        );

        $session = $this->get('session');

        if (!empty($shop)) {
            $session->set('seoshop_id', $shop->getId());
            return $this->redirect(
                $this->generateUrl('seoshop_settings')
            );
        }

        $language = $request->get('language', 'en');
        $timestamp = $request->get('timestamp');
        $token = $request->get('token');
        $signature = $request->get('signature');

        $shop = $this->get('seoshop.webservice')->install($language, $shopId, $timestamp, $token, $signature);
        if (!$shop) {
            return $this->render(
                'AppBundle::errors/missing_seoshop.html.twig',
                [
                    'api_key' => $this->container->getParameter('seoshop.api_key'),
                    'language' => $language
                ]
            );
        }

        $session->set('seoshop_id', $shop->getId());

        return $this->redirect(
            $this->generateUrl(
                $this->container->getParameter('seoshop.success_route'),
                [
                    'seoshopId' => $shop->getSeoshopId(),
                    'seoshopApiToken' => $shop->getSeoshopApiToken()
                ]
            )
        );
    }


    /**
     * Shows authentication form where you can give in your
     * messagebird api_key for validation. We will validate the
     * given input and save it for your shop when we're sure your key works.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function authenticateAction(Request $request)
    {
        $messagebirdService = $this->get('messagebird.webservice');
        $seoshopService = $this->get('seoshop.webservice');
        $session = $this->get('session');
        $seoshopId = $session->get('seoshop_id');
        $em = $this->getDoctrine()->getManager();

        $shopRepo = $em->getRepository('AppBundle:SeoshopShop');
        $seoshop = $shopRepo->find($seoshopId);

        if (!$seoshop) {
            return $this->render(
                'AppBundle::errors/missing_seoshop.html.twig',
                [
                    'api_key' => $this->container->getParameter('seoshop.api_key'),
                    'language' => $request->get('language', 'en')
                ]
            );
        }
        $accountRepo = $em->getRepository('AppBundle:MessagebirdAccount');
        $account = $accountRepo->findOneBySeoshopShop($seoshop);
        if (!$account) {
            $account = $seoshopService->createMessagebirdAccount($seoshop);
        }

        $form = $this->createMessagebirdAccountForm($account);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $account = $form->getData();
            $keyIsValid = $messagebirdService->authenticateApiKey($account->getApiKey());
            if (!$keyIsValid) {
                return $this->redirect($this->generateUrl('seoshop_authenticate_key'));
            }

            $seoshopService->InitializeShop($seoshop);
            $em->persist($account);
            $session->getFlashBag()->add('success', 'Configuration saved successfull!');
            $em->flush();

            // return $this->redirect("http://seoshop.webshopapp.com/backoffice/signin/");
            return $this->redirect($this->generateUrl('seoshop_settings') . '?firstrun=1');
        }

        return $this->render(
            'AppBundle::auth.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @param MessagebirdAccount $messagebirdAccount
     * @return \Symfony\Component\Form\Form
     */
    public function createMessagebirdAccountForm(MessagebirdAccount $messagebirdAccount)
    {
        $form = $this->createFormBuilder($messagebirdAccount)
            ->add('api_key', TextType::class, array(
                'label' => 'MessageBird API Key '
            ))
            ->add('sender', TextType::class, array(
                'label' => 'Displayed sender',
                'data' => $messagebirdAccount->getContactName() ? $messagebirdAccount->getContactName() : "",
                'attr' => ['maxlength' => 11],
            ))
            ->add('contactName', TextType::class, array(
                'label' => 'Contact name  ',
                'data' => $messagebirdAccount->getContactName() ? $messagebirdAccount->getContactName() : "",
            ))
            ->add('contactNumber', TextType::class, array(
                'label' => 'Contact mobile ',
                'data' => $messagebirdAccount->getContactNumber() ? $messagebirdAccount->getContactNumber() : "",
            ))
            // TODO: make dropdown
            ->add('country', TextType::class, array(
                'label' => 'Country code',
                'data' => $messagebirdAccount->getCountry() ? $messagebirdAccount->getCountry() : "",
            ))
            ->add('submit', SubmitType::class, array('label' => "Let's go!"));

        return $form->getForm();
    }

    /**
     * Displays configuration for shop.
     *
     * @return Response
     */
    public function settingsAction(Request $request)
    {
        $session = $this->get('session');
        if ($request->request->has('seoshop_action')) {
            $this->getActionsFormAndHandleIt($request);
        }
        $em = $this->getDoctrine()->getManager();
        if (!$session->has('seoshop_id')) {
            return $this->redirectToRoute('seoshop_install_success');
        }
        $shop = $em->getRepository("AppBundle:SeoshopShop")->findOneBy(array(
            'id' => $session->get('seoshop_id')
        ));

        $msgBird = $em->getRepository('AppBundle:MessagebirdAccount')
            ->findOneBy(array('seoshopShop' => $shop->getId()));
        if (!$msgBird) {
            $msgBird = new MessagebirdAccount();
        }
        $msgForm = $this->createForm(MessagebirdAccountType::class, $msgBird);
        $msgForm->handleRequest($request);
        if ($msgForm->isValid()) {
            $msgBird = $msgForm->getData();
            $msgBird->setSeoshopShop($shop);
            $em->persist($msgBird);
            $em->flush();
            $session->getFlashBag()->add('success', 'Configuration has been saved.');
            return $this->redirectToRoute('seoshop_settings');
        }

        $bulkForm = $this->createForm(BulkMessageType::class, new BulkMessage());
        $actionsForm = $this->getActionsFormAndHandleIt($request);

        return $this->render(
            'AppBundle::settings.html.twig',
            array(
                'balance' => $this->getCredits($msgBird),
                'userForm' => $msgForm->createView(),
                'actionsForm' => $actionsForm,
                'bulkForm' => $bulkForm->createView(),
                'userName' => $msgBird->getSender()
            )
        );
    }

    /**
     * @param $request
     * @return ArrayCollection
     */
    public function getActionsFormAndHandleIt($request)
    {
        $session = $this->get('session');
        $shopId = $session->get("seoshop_id");
        $em = $this->getDoctrine()->getManager();
        $seoshop = $em->getRepository('AppBundle:SeoshopShop')->find($shopId);

        $actionBag = new ArrayCollection();

        $actions = $this
            ->getDoctrine()
            ->getRepository('AppBundle:SeoshopAction')
            ->findBy(array('seoshopShop' => $seoshop,));

        foreach ($actions as $action) {
            $form = $this->createForm(SeoshopActionType::class, $action);
            $form->handleRequest($request);
            $actionBag->add($form->createView());
        }

        return $actionBag;
    }

    /**
     * @param MessagebirdAccount $mbird
     * @return $this
     */
    protected function getCredits(MessagebirdAccount $mbird)
    {
        $cli = new Client();
        $key = $mbird->getApiKey();
        if ($key == 'test_KvAcRhLoZEcgyMO9xqs3oFEsD') {
            $key = 'live_UHTW9TGTg4srEZlvQsG0gYKW4';

        }

        $cli->setAccessKey($key);

        return $cli->balance->read();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function uninstallAction(Request $request)
    {
        $shopId = $request->get('shop_id', null);

        $shop = $this->get('doctrine')->getManager()->getRepository('AppBundle:SeoshopShop')->findOneBy(
            [
                'seoshopId' => $shopId,
                'enabled' => true
            ]
        );
        if (!$shop) {
            return $this->render(
                'AppBundle::errors/missing_seoshop.html.twig',
                [
                    'api_key' => $this->container->getParameter('seoshop.api_key'),
                    'language' => $request->get('language', 'en')
                ]
            );
        }

        $this->get('seoshop.webservice')->uninstall($shop);

        return $this->render(
            'AppBundle::uninstall.html.twig',
            [
                'api_key' => $this->container->getParameter('seoshop.api_key'),
                'language' => $shop->getSeoshopLanguage()
            ]
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function saveShopAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $validator = $this->get('validator');
        $seoshopId = $this->get('session')->get('seoshop_id');
        $_seoshopAction = $request->request->get('seoshop_action');
        $seoshopAction = null;

        /* use ID to fetch object from database */
        if ($_seoshopAction['id']) {
            $seoshopAction = $em->getRepository('AppBundle:SeoshopAction')->find($_seoshopAction['id']);
        }

        /* create new object if result is still NULL */
        if (!$seoshopAction) {
            $errors = 'Unable to save data, if this problem persists, please reinstall the app.';
        }

        /* fill object with post data */
        if (array_key_exists('message', $_seoshopAction)) {
            $seoshopAction->setMessage($_seoshopAction['message']);
        }

        $seoshopAction->setToOwner(false);
        $seoshopAction->setToClient(false);
        if (array_key_exists('toClient', $_seoshopAction)) {
            $seoshopAction->setToClient(true);
        }
        if (array_key_exists('toOwner', $_seoshopAction)) {
            $seoshopAction->setToOwner(true);
        }
        /* validate object and store errors */
        $errors = $validator->validate($seoshopAction);

        if (!count($errors)) {
            /* save to db */
            $em->persist($seoshopAction);
            $em->flush();

            /* return no-content success header */
            $view = $this->view(null, 204);

        } else {

            /* return validation errors */
            $view = $this->view($errors, 400);
        }

        /* return response */
        $view->setFormat('json');
        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param \DateTime $from
     * @param \DateTime $to
     * @return bool|Response
     */
    public function bulkAction(Request $request, \DateTime $from, \DateTime $to)
    {
        $collection = array();
        $shop = $this->getDoctrine()->getRepository('AppBundle:SeoshopShop')->findOneBy([
            'id' => $this->get('session')->get('seoshop_id')
        ]);
        $client = $shop->createApiClient();
        if (!$shop) {
            return false;
        }

        $range = array(
            'created_at_min' => $from->format("Y-m-d H:i:s"),
            'created_at_max' => $to->format("Y-m-d H:i:s"),
        );
        $orders = $client->orders->get(null, $range);
        foreach ($orders as $key => $order) {
            $number = $order['mobile'];
            if (empty($number)) {
                $number = $order['phone'];
            }
            if ($order['number']) {
                $collection[] = array(
                    'order_id' => $order['number'],
                    'customer_name' => $order['firstname'] . ' ' . $order['lastname'],
                    'created_at' => $order['createdAt'],
                    'mobile' => $number,
                );
            }
        }

        $response = new Response(json_encode(array('orders' => $collection)));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Accepts params from bulkform to send bulkmessages
     * @param Request $request
     * @return Response
     */
    public function createBulkAction(Request $request)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $wsHelper = $this->get('messagebird.webservice');
        $flashbag = $this->get('session')->getFlashBag();

        $from = new \DateTime($data['from']);
        $to = new \DateTime($data['to']);
        $msg = $data['message'];
        $shop = $em->getRepository('AppBundle:SeoshopShop')->find($data['shopId']);
        $account = $em->getRepository('AppBundle:MessagebirdAccount')->findOneBy(['seoshopShop' => $shop->getId()]);
        $wsHelper->client->setAccessKey($shop->getSeoshopApiKey());
        $client = $shop->createApiClient();
        $range = array(
            'created_at_min' => $from->format("Y-m-d H:i:s"),
            'created_at_max' => $to->format("Y-m-d H:i:s"),
        );

        $bulkMessage = new BulkMessage();
        $bulkMessage->setArticleCode("-");
        $bulkMessage->setMessage($msg);
        $bulkMessage->setOrderFromDate($from);
        $bulkMessage->setOrderToDate($to);
        $bulkMessage->setSeoshopShop($shop);
        $em->persist($bulkMessage);

        // Range of orders array, now create messages
        $ordersRange = $client->orders->get(null, $range);
        $responses = array();
        foreach ($ordersRange as $order) {
            $country = $order['addressBillingCountry'];
            $number = $order['mobile'];
            if (empty($number)) {
                $number = $order['phone'];
            }
            $message = new MessageModel();
            $message->setFirstName($order['firstname']);
            $message->setLastName($order['lastname']);
            $message->setCountryCode($country['code']);
            $message->setRawPhoneNumber($number);
            $message->setPhoneNumber($number);
            $message->setOrderNumber($order['number']);
            $message->setSender($account->getSender());
            $message->setSenderName($account->getContactName());
            $message->setRawSenderNumber($account->getContactNumber());
            $message->setSenderNumber($account->getContactNumber());
            $message->setSenderCountry($account->getCountry());
            // Add tnt code
            $number = preg_replace('/\D/', '', $message->getPhoneNumber());
            $message->setPhoneNumber($number);

            $send = $wsHelper->sendBulkMessage($message, $bulkMessage, $number, $account->getApiKey());
            if ($send) {
                $responses[] = 'success fully send to ' . $message->getPhoneNumber();
            } else {
                $responses[] = 'failed to send to ' . $message->getPhoneNumber();
            }
//            $flashbag->add('success', "Een bericht is verstuurd ".$message->getPhoneNumber());
        }

        $em->flush();
        $response = new Response(json_encode(array('items' => $responses)));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
