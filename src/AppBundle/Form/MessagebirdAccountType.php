<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class MessagebirdAccountType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('seoshopShop', 'Symfony\Component\Form\Extension\Core\Type\HiddenType')
            ->add('api_key',  'Symfony\Component\Form\Extension\Core\Type\TextType')
            ->add('sender', 'Symfony\Component\Form\Extension\Core\Type\TextType', [
                'label' => 'Displayed Sender',
                'attr' => ['maxlength' => 11]
            ])
            ->add('contactName', 'Symfony\Component\Form\Extension\Core\Type\TextType')
            ->add('contactNumber')
            ->add('country')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', [
                'label' => 'Save'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\MessagebirdAccount'
        ));
    }
}
