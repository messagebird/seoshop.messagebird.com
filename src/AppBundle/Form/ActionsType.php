<?php

namespace AppBundle\Form;

use AppBundle\Entity\SeoshopAction;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActionsType extends AbstractType
{
    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array                                        $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('actions', 'Symfony\Component\Form\Extension\Core\Type\CollectionType', [
            'label' => 'Actions',
            'entry_type' => SeoshopActionType::class,
            'allow_add' => true,
            'allow_delete' => false,
            'prototype' => false
        ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\SeoshopShop'
        ));
    }

    public function getName()
    {
        return 'Action';
    }
}
