<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActionsCollectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('seoshopName');

        $builder->add('actions', CollectionType::class, array(
            'entry_type' => ActionsType::class,
            'allow_add' => true
        ));

        $builder->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', [
            'label' => 'Wijziging opslaan'
        ]);


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\SeoshopShop'
        ));
    }

    public function getName()
    {
        return 'ShopActionCollection';
    }
}
