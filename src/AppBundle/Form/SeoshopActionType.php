<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SeoshopActionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', 'Symfony\Component\Form\Extension\Core\Type\HiddenType')
            ->add('seoshopHook', 'Symfony\Component\Form\Extension\Core\Type\HiddenType')
            ->add('toOwner', 'Symfony\Component\Form\Extension\Core\Type\CheckboxType', array(
                'label' => 'Owner',
                'required' => false,
                'value' => 0
            ))
            ->add('toClient', 'Symfony\Component\Form\Extension\Core\Type\CheckboxType', array(
                'label' => 'Customer',
                'required' => false,
                'value' => 0
            ))
            ->add('message', 'Symfony\Component\Form\Extension\Core\Type\TextareaType', array(
                'required' => false,
                'attr' => [
                    'rows' => 6,
                    'placeholder' => 'Write your message...',
                    'disabled' => 'true'
                ]
            ))
            ->add('actionSubmit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array(
                'label' => 'Save',
                'disabled' => 'true'
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\SeoshopAction'
        ));
    }
}
