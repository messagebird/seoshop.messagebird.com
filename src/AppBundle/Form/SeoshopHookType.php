<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SeoshopHookType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('seoshopApiKey')
            ->add('seoshopApiToken')
            ->add('seoshopLanguage')
            ->add('seoshopName')
            ->add('isActive')
            ->add('updatedAt', 'Symfony\Component\Form\Extension\Core\Type\DateType')
            ->add('createdAt', 'Symfony\Component\Form\Extension\Core\Type\DateType')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\SeoshopHook'
        ));
    }
}
