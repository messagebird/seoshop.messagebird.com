<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BulkMessageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('orderFromDate', 'Symfony\Component\Form\Extension\Core\Type\DateType',[
                'widget' => 'single_text',
                'attr' => [
                    'value' => date('Y-m-d', strtotime("-2 days", time()))
                ]
            ])
            ->add('orderToDate', 'Symfony\Component\Form\Extension\Core\Type\DateType', [
                'widget' => 'single_text',
                'attr' => [
                    'value' => date('Y-m-d')
                ]
            ])
            ->add('message', null, [
                'attr' => [
                    'placeholder' => 'Message to send to customers'
                ]
            ])
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\BulkMessage'
        ));
    }
}
