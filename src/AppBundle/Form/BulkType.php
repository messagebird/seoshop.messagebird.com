<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BulkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('orderFromDate');
        $builder->add('orderToDate');
        $builder->add('message');
        $builder->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\BulkMessage'
        ));
    }

    public function getName()
    {
        return 'app_bundle_bulk_type';
    }
}
