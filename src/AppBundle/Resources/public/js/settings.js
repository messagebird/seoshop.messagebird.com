// Bulk message check action
function bulkMsgCheck(){
    $("tbody#bulk-body").empty();
    var from = $("#bulk_message_orderFromDate").val();
    var to = $("#bulk_message_orderToDate").val();
    var bulkHolder = $("#bulkholder").removeClass("hidden");
    $.get("/api/bulk/"+from+"/"+to, function(data, status){
        $(this).parent().find(".hidden").removeClass("hidden");
        for (var i = 0; i < data.orders.length; i++) {
            var createDate = new Date(Date.parse(data.orders[i]["created_at"]));
            createDate.getDate("Y-m-h");
            $('.bulk-list').append('<tr>' +
                '<td>'+data.orders[i]["order_id"]+'</td>' +
                '<td>'+data.orders[i]["customer_name"]+'</td>' +
                '<td>'+data.orders[i]["mobile"]+'</td>' +
                '<td>'+createDate.toDateString()+'</td>' +
                '</tr>');
        }
    });
}

// Gets GET value's in url
function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
}

// Do stuff for document.ready
$(document).ready(function () {

    $('#form_api_key').focus();

    $('.alert').next().addClass("error");

    // First run popup
    if ($('body').hasClass('settings')) {
        if(getQueryVariable("firstrun")) {
            window.history.pushState('','','/seoshop/settings');
            $(".settings .lightbox").css("display", "block");
            $(".settings .lightbox button").click(function (e) {
                e.preventDefault();
                $(".settings .lightbox").css("display","none");
            });
        }
    }

    // Settings formulier fixes
    $(".settings .form-group:has(.checkbox)").css("display", "inline-block");
    $(".settings .form-group .checkbox label ").append("<span></span>");

    // Menu script
    $(".menu li").click(function (e) {
        e.preventDefault();
        $(".settings #wrapper").css("display","block");
        $(".menu .active").removeClass("active");
        $(this).addClass("active");
    });
    $(".tabs li").click(function (e) {
        e.preventDefault();
        $(".tabs li").removeClass("active");
        $(this).addClass("active");
    });
    $(".itemThree").click(function (e) {
        e.preventDefault();
        $(".settings #wrapper").css("display","none");
    });

    // Ajax submission settings forms
    $('form[name="seoshop_action"]').submit(function() {
        var submitButton = this.getElementsByClassName('btn-default')[0];
        var loading = $(this).find('.loading').html();
        var done = $(this).find('.done').html();
        var ownerCheckbox = $(this).find("#seoshop_action_toOwner")[0]['checked'];
        var clientCheckbox = $(this).find("#seoshop_action_toClient")[0]['checked'];

        $(this).ajaxSubmit({
            success: function() {
                $(submitButton).html(done);
                setTimeout(function() {
                    $(submitButton).html(loading);
                }, 2000);
                if(!ownerCheckbox && !clientCheckbox) {
                    submitButton.disabled = true;
                }
            }
        });
        return false;
    });

    $('form[name="messagebird_account"]').submit(function () {
        $(this).ajaxSubmit({
            success: function() {
                console.log('done');
            }
        })
    });

    $( 'form[name="bulk_message"]' ).submit(function ( e ) {
        console.log('test');
        document.getElementById('bulk_message_submit').disabled = 'true';
        e.preventDefault();
        $("tbody#bulk-body").empty();
        var from = $("#bulk_message_orderFromDate").val();
        var to = $("#bulk_message_orderToDate").val();
        var bulkHolder = $("#bulkholder").removeClass("hidden");
        $.get("/api/bulk/"+from+"/"+to, function(data, status){
            document.getElementById('bulk_message_submit').disabled = null;
            var orderDate = new Date(data.orders[0]["created_at"]);
            $(this).parent().find(".hidden").removeClass("hidden");
            for (var i = 0; i < data.orders.length; i++) {
                var createDate = new Date(data.orders[i]['created_at']);
                $('.bulk-list').append('<tr id="bOrders" value='+data.orders[i]["order_id"]+'>' +
                    '<td id="order_id">'+data.orders[i]["order_id"]+'</td>' +
                    '<td>'+data.orders[i]["customer_name"]+'</td>' +
                    '<td>'+data.orders[i]["mobile"]+'</td>' +
                    '<td>'+ createDate.toDateString() +'</td>' +
                    '</tr>');
            }
        });
    });

    // Settings form initial action
    var settingsForms = document.getElementsByClassName('settings-form');
    Array.prototype.forEach.call(settingsForms, function(settingsForm) {
        var enabled = false;
        var checkBoxes = settingsForm.getElementsByClassName('checkbox');
        Array.prototype.forEach.call(checkBoxes, function(checkBox) {
            if(checkBox.getElementsByTagName('input')[0]['checked']) {
                enabled = true;
            }
        });
        var textArea = settingsForm.getElementsByClassName('form-control')[0];
        var button = settingsForm.getElementsByClassName('btn-default')[0];
        if(!enabled) {
            textArea.disabled = true;
            button.disabled = true;
        } else {
            textArea.disabled = false;
            button.disabled= false;
        }
    });

    $('#seoshop_action_toOwner, #seoshop_action_toClient').click(function (e) {
        var ownerCheckbox = $(this).parent().parent().parent().parent().find("#seoshop_action_toOwner")[0]['checked'];
        var clientCheckbox = $(this).parent().parent().parent().parent().find("#seoshop_action_toClient")[0]['checked'];

        if ( ownerCheckbox || clientCheckbox ) {
            $(this).parent().parent().parent().parent().find(".submit-w-loader").children().children().prop("disabled", false);
            $(this).parent().parent().parent().parent().find(".form-group").children("#seoshop_action_message").prop("disabled", false);
        }
        else {
            // $(this).parent().parent().parent().parent().find(".submit-w-loader").children().children().prop("disabled", true);
            $(this).parent().parent().parent().parent().find(".form-group").children("#seoshop_action_message").prop("disabled", true);
        }
    });
});
