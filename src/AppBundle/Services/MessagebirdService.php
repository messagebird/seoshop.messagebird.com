<?php

namespace AppBundle\Services;

use AppBundle\Entity\BulkMessage;
use AppBundle\Entity\MessagebirdAccount;
use AppBundle\Entity\SeoshopAction;
use AppBundle\Entity\SeoshopHook;
use AppBundle\Entity\SeoshopShop;
use AppBundle\Model\Message;
use Doctrine\ORM\EntityManagerInterface;
use MessageBird\Client;
use MessageBird\Exceptions\AuthenticateException;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Created by PhpStorm.
 * User: nav
 * Date: 03-02-16
 * Time: 14:58
 */
class MessagebirdService
{
    /**
     * @var Client
     */
    public $client;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * MessagebirdService constructor.
     * @param EntityManagerInterface $em
     * @param Session $session
     * @param Logger $logger
     */
    public function __construct(EntityManagerInterface $em, Session $session, Logger $logger)
    {
        $this->em = $em;
        $this->client = new Client();
        $this->session = $session;
        $this->logger = $logger;
    }

    /**
     * @param string $apiKey
     * @return bool
     */
    public function authenticateApiKey($apiKey=null)
    {
        if (empty($apiKey)) {
            return false;
        }
        $this->client->setAccessKey($apiKey);
        try {
            $balance = $this->client->balance->read();
        } catch (AuthenticateException $e) {
            $this->session->getFlashBag()->add('error-invalid-api-key', 'This is not a valid key.');
            return false;
        }
        if (!isset($balance->amount)) {
            return false;
        }
        return true;
    }

    /**
     * @param Message $message
     * @param SeoshopShop $shop
     * @param SeoshopHook $hook
     * @return bool|Response
     * @throws \Exception
     */
    public function sendOrderMessage(Message $message, SeoshopShop $shop, SeoshopHook $hook, $apiKey)
    {
        $this->client->setAccessKey($apiKey);
        $actionRepo = $this->em->getRepository('AppBundle:SeoshopAction');

        if($hook->getItemAction() == 'shipped') {
            $actionType = SeoshopAction::ORDER_SHIPPED;
            if(empty($message->getTntCode())) {
                $actionType = SeoshopAction::ORDER_SHIPPED_NOTNT;
            }
        }

        /**
         * @var SeoshopAction $action
         */
        if(isset($actionType)) {
            $action = $actionRepo->findOneBy(array(
                'seoshopShop' => $shop,
                'seoshopHook' => $hook,
                'action' => $actionType,
            ));
        } else {
            $action = $actionRepo->findOneBy(array(
                'seoshopShop' => $shop,
                'seoshopHook' => $hook,
                ));
        }
        if (!$action) {
            return false;
        }
        if (!$action->isToClient() && !$action->isToOwner()) {
            return true;
        }

        if($action->getAction()==SeoshopAction::ORDER_UPDATED && $message->getOrderStatus() != 'cancelled') {
            $this->logger->addCritical('Cancelled hook: Order updated but not cancelled.');
            return true;
        }

        $send1 = true;
        $send2 = true;
        if ($action->isToClient()) {
            $number = preg_replace('/\D/', '', $message->getPhoneNumber());
            $message->setPhoneNumber($number);
            $send1 = $this->sendMessage($message, $action, $number);
        }
        if ($action->isToOwner()) {
            $number = preg_replace('/\D/', '', $message->getSenderNumber());
            $message->setSenderNumber($number);
            $send2 = $this->sendMessage($message, $action, $number);
        }

        if($send1 && $send2) {
            return true;
        }
        return false;
    }

    /**
     * // TODO: Refactor from order to product
     * @param Message $message
     * @param SeoshopShop $shop
     * @param SeoshopHook $hook
     * @return bool|Response
     * @throws \Exception
     */
    public function sendProductMessage(Message $message, SeoshopShop $shop, SeoshopHook $hook, $apiKey)
    {
        $this->client->setAccessKey($apiKey);
        $actionRepo = $this->em->getRepository('AppBundle:SeoshopAction');
        $articleCodes = array();

        /**
         * @var SeoshopAction $action
         */
        $action = $actionRepo->findOneBy(array(
            'seoshopShop' => $shop,
            'seoshopHook' => $hook,
        ));
        if (!$action) {
            return false;
        }
        if (!$action->isToOwner()) {
            return true;
        }

        $api = $shop->createApiClient();
        $variants = $api->variants->get(null, array('product' => $message->getProduct(),));
        if(empty($variant)) {
            $this->logger->addCritical('Unable to retrieve variant for product '.$message->getProduct());
            return false;
        }

        foreach($variants as $variant) {
            if(($variant['stockTracking'] == 'disabled') or ($variant['stockLevel'] > 0)) {
                continue;
            }

            $articleCodes[] = $variant['articleCode'];
        }

        $send = true;
        if (!empty($articleCodes)) {
            $message->setProduct(join(', ', $articleCodes));
            $number = preg_replace('/\D/', '', $message->getSenderNumber());
            $message->setSenderNumber($number);
            $send2 = $this->sendMessage($message, $action, $number);
        }

        if($send) {
            return true;
        }
        return false;
    }

    /**
     * @param Message $message
     * @param SeoshopAction $action
     * @param string $number
     * @return bool
     */
    private function sendMessage(Message $message, SeoshopAction $action, $number)
    {
        $messageBirdMessage = new \MessageBird\Objects\Message();
        $messageBirdMessage->recipients = $number;
        $messageBirdMessage->body = $message->parse($action->getMessage());
        $messageBirdMessage->originator = $message->getSender();
        $messageBirdMessage->reference = "mb_seoshop_order_update";
        if (empty($message->getSender())) {
            $messageBirdMessage->originator = $message->getSenderNumber();
        }
        $response = $this->client->messages->create($messageBirdMessage);
        if ($response->recipients->items[0]->status == 'sent' && $response->recipients->totalCount == $response->recipients->totalSentCount) {
            $messageEntity = new \AppBundle\Entity\Message();
            $messageEntity->setClientData(serialize($message));
            $messageEntity->setMessage($message->getMessage());
            $messageEntity->setPhoneNumber($number);
            $messageEntity->setShopAction($action);
            $this->em->persist($messageEntity);
            $this->em->flush();
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Message $message
     * @param BulkMessage $bulkMessage
     * @param string $number
     * @param string $apiKey
     * @return bool
     */
    public function sendBulkMessage(Message $message, BulkMessage $bulkMessage, $number, $apiKey)
    {
        $this->client->setAccessKey($apiKey);
        $messageBirdMessage = new \MessageBird\Objects\Message();
        $messageBirdMessage->recipients[] = $number;
        $messageBirdMessage->body = $message->parse($bulkMessage->getMessage());
        $messageBirdMessage->originator = $message->getSender();
        $messageBirdMessage->reference = "mb_seoshop_bulk";
        if (empty($message->getSender())) {
            $messageBirdMessage->originator = $message->getSenderNumber();
        }
        $response = $this->client->messages->create($messageBirdMessage);
        if ($response->recipients->items[0]->status == 'sent' && $response->recipients->totalCount == $response->recipients->totalSentCount) {
            $messageEntity = new \AppBundle\Entity\Message();
            $messageEntity->setClientData(serialize($message));
            $messageEntity->setMessage($message->getMessage());
            $messageEntity->setPhoneNumber($number);
            $messageEntity->setBulkMessage($bulkMessage);
            $this->em->persist($messageEntity);
            $this->em->flush();
            return true;
        } else {
            return false;
        }
    }
}
