<?php
/**
 * Created by Notive.
 * User: thomas
 * Date: 02/03/16
 * Time: 12:00
 */

namespace AppBundle\Services;

use AppBundle\Entity\MessagebirdAccount;
use AppBundle\Entity\SeoshopAction;
use AppBundle\Entity\SeoshopHook;
use AppBundle\Entity\SeoshopShop;
use AppBundle\Model\Message;
use Doctrine\ORM\EntityManagerInterface;
use MessageBird\Client;
use MessageBird\Exceptions\AuthenticateException;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Notive\SEOshopBundle\Helper\WebshopManager;

class SeoshopService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var WebshopManager
     */
    private $webshopManager;

    /**
     * SeoshopService constructor.
     * @param EntityManagerInterface $em
     * @param Session $session
     * @param Logger $logger
     * @param WebshopManager $webshopManager
     */
    public function __construct(EntityManagerInterface $em, Session $session, Logger $logger, WebshopManager $webshopManager)
    {
        $this->em = $em;
        $this->client = new Client();
        $this->session = $session;
        $this->logger = $logger;
        $this->webshopManager = $webshopManager;
    }

    /**
     * @param string $language
     * @param string $shopId
     * @param string $timestamp
     * @param string $token
     * @param string $signature
     * @return SeoshopShop|bool
     */
    public function install($language, $shopId, $timestamp, $token, $signature)
    {
        $isValid = $this->webshopManager->validateInstallSignature($language, $shopId, $timestamp, $token, $signature);
        if (!$isValid) {
            return false;
        }

        /**
         * @var SeoshopShop $webshop
         */
        $webshop = $this->webshopManager->getRepository()->findOneBySeoshopId($shopId);
        if (!$webshop) {
            $webshop = $this->webshopManager->createWebshop();
            $webshop->setSeoshopId($shopId);
            $webshop->setIsActive(false);
        }
        $webshop->setSeoshopLanguage($language);
        $webshop->setSeoshopApiToken($this->webshopManager->generateApiToken($token));
        $this->em->persist($webshop);

        $api = $webshop->createApiClient();
        $shopData = $api->shop->get();
        $webshop->setSeoshopName($shopData['mainDomain']);
        $this->em->persist($webshop);

        $this->em->flush();
        return $webshop;
    }

    /**
     * @param SeoshopShop $shop
     * @return SeoshopShop
     */
    public function uninstall(SeoshopShop $shop)
    {
        $shop->setEnabled(false);
        $this->em->persist($shop);
        $this->em->flush();
        return $shop;
    }

    /**
     * @param SeoshopShop $seoshop
     * @return MessagebirdAccount
     */
    public function createMessagebirdAccount($seoshop)
    {
        $api = $seoshop->createApiClient();
        $company = $api->shopCompany->get();

        $sender = $company['name'];
        if(strlen($sender) > 11) {
            $sender = substr($sender,0,11);
        }

        $account = new MessagebirdAccount();
        $account->setSeoshopShop($seoshop);
        $account->setSender($sender);
        $account->setContactName($company['name']);
        $account->setContactNumber($company['phone']);
        $account->setCountry($company['country']['code']);

        $this->em->persist($account);
        $this->em->flush();
        return $account;
    }


    /**
     * @param SeoshopShop $shop
     */
    public function initializeShop(SeoshopShop $shop)
    {
        $api = $shop->createApiClient();
        $hookRepo = $this->em->getRepository('AppBundle:SeoshopHook');
        $actionRepo = $this->em->getRepository('AppBundle:SeoshopAction');

        $defaultHooks = SeoshopHook::getDefaultActions();

        $apiwebhooks = $api->webhooks->get();
        foreach($apiwebhooks as $apiwebhook) {
            $api->webhooks->delete($apiwebhook['id']);
        }

        foreach($defaultHooks as $defaultHook) {
            $hook = $hookRepo->findOneBy(array(
                'seoshopShop' => $shop,
                'itemGroup' => $defaultHook['itemGroup'],
                'itemAction' => $defaultHook['itemAction'],
            ));
            if($hook) {
                $hook->setEnabled(true);
                $this->em->persist($hook);
            }
            if(!$hook) {
                $hook = new SeoshopHook();
                $hook->setSeoshopShop($shop);
                $hook->setItemGroup($defaultHook['itemGroup']);
                $hook->setItemAction($defaultHook['itemAction']);
                $hook->setEnabled(true);
                $this->em->persist($hook);
            }
            $webhook = $this->createHook($api, $shop, $hook, $defaultHook);
            $hook->setHookId($webhook['id']);
            $this->em->persist($hook);
            if($defaultHook['label'] == 'order_shipped') {
                $tntAction = $actionRepo->findOneBy(array(
                    'seoshopShop' => $shop,
                    'seoshopHook' => $hook,
                    'action' => SeoshopAction::ORDER_SHIPPED,
                ));
                if(!$tntAction) {
                    $tntAction = new SeoshopAction();
                    $tntAction->setSeoshopShop($shop);
                    $tntAction->setSeoshopHook($hook);
                    $tntAction->setAction(SeoshopAction::ORDER_SHIPPED);
                    $this->em->persist($tntAction);
                }
                $noTntAction = $actionRepo->findOneBy(array(
                    'seoshopShop' => $shop,
                    'seoshopHook' => $hook,
                    'action' => SeoshopAction::ORDER_SHIPPED,
                ));
                if(!$noTntAction) {
                    $noTntAction = new SeoshopAction();
                    $noTntAction->setSeoshopShop($shop);
                    $noTntAction->setSeoshopHook($hook);
                    $noTntAction->setAction(SeoshopAction::ORDER_SHIPPED_NOTNT);
                    $this->em->persist($noTntAction);
                }
            } else {
                $action = $actionRepo->findOneBy(array(
                    'seoshopShop' => $shop,
                    'seoshopHook' => $hook,
                    'action' => $defaultHook['id'],
                ));
                if(!$action) {
                    $action = new SeoshopAction();
                    $action->setSeoshopShop($shop);
                    $action->setSeoshopHook($hook);
                    $action->setAction($defaultHook['id']);
                    $this->em->persist($action);
                }
            }
        }
        $shop->setEnabled(true);
        $this->em->persist($shop);

        $this->em->flush();
    }

    /**
     * @param \WebshopappApiClient $api
     * @param SeoshopShop $shop
     * @param SeoshopHook $hook
     * @param array $defaultHook
     * @return array
     */
    private function createHook(\WebshopappApiClient $api, SeoshopShop $shop, SeoshopHook $hook, array $defaultHook)
    {
        $webhook = $api->webhooks->create(array(
            'isActive' => true,
            'itemGroup' => $defaultHook['itemGroup'],
            'itemAction' => $defaultHook['itemAction'],
            'language' => $shop->getSeoshopLanguage(),
            'format' => 'json',
            'address' => $this->getUrl('api/'.$defaultHook['itemGroup'].'/'.$shop->getId().'/'.$hook->getId()),
        ));
        $this->logger->addDebug('Creating api with url: '.$this->getUrl('api/'.$defaultHook['itemGroup'].'/'.$shop->getId().'/'.$hook->getId()));
        return $webhook;
    }

    /**
     * @param null $path
     * @return string
     */
    private function getUrl($path = null)
    {
        if ($path) {
            $url = $_SERVER['HTTP_HOST'] . '/' . $path;
        } else {
            $url = $_SERVER['HTTP_HOST'];
        }

        $url = "https://" . $url;

        return $url;
    }
}