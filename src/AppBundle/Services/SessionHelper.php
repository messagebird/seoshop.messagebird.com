<?php
/**
 * Created by PhpStorm.
 * User: sub
 * Date: 16-02-16
 * Time: 12:20
 */

namespace AppBundle\Services;


use AppBundle\Entity\SeoshopAction;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;

class SessionHelper
{
    protected $em;

    /**
     * SessionHelper constructor.
     */
    public function __construct(EntityManager $entityManager, Session $session)
    {
        $this->em = $entityManager;
        $this->session = $session;
    }

    public function getMessageBirdAccount()
    {
        $session_seoshop = $this->session->get('seoshop_id');
        $shop = $this->em->getRepository('AppBundle:MessagebirdAccount')
            ->findOneBy(array('seoshopShop', $session_seoshop));

        if($shop){
            return $shop;
        }

        return false;
    }

    public function getActions()
    {
        $session_id = $this->session->get('seoshop_id');
        $actions = $this->em->getRepository('AppBundle:SeoshopAction')
            ->findBy(array('seoshopShop' => $session_id));

        if($actions){
            return $actions;
        }

        return false;
    }

    public function createAllActions()
    {
        $actionBag = array();
        $seoshop_id = $this->session->get('seoshop_id');
        $defaults = SeoshopAction::getDefaultActions();

        foreach ($defaults as $key => $default) {
            if($key == 0) $key++;
            $action = new SeoshopAction(
                $this->session->get('seoshop_id'),
                $key
            );
            $this->em->persist($action);
            $actionBag[] = $action;
        }
        $this->em->flush();

        return $actionBag;
    }
}