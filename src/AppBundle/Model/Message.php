<?php
namespace AppBundle\Model;
use AppBundle\Entity\MessagebirdAccount;

/**
 * Class Message
 * @package AppBundle\Model
 */
class Message
{
    /**
     * @var string
     */
    protected $phoneNumber;

    /**
     * @var string
     */
    protected $rawPhoneNumber;

    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var string
     */
    protected $tntCode;

    /**
     * @var string
     */
    protected $orderNumber;

    /**
     * @var string
     */
    protected $countryCode;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var string
     */
    protected $sender;

    /**
     * @var string
     */
    protected $senderName;

    /**
     * @var string
     */
    protected $senderNumber;

    /**
     * @var string
     */
    protected $rawSenderNumber;

    /**
     * @var string
     */
    protected $senderCountry;

    /**
     * @var string
     */
    protected $rawText;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var string
     */
    protected $orderStatus;

    /**
     * @var string
     */
    protected $product;

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     * @return Message
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Message
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return Message
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTntCode()
    {
        return $this->tntCode;
    }

    /**
     * @param string $tntCode
     * @return Message
     */
    public function setTntCode($tntCode)
    {
        $this->tntCode = $tntCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return Message
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     * @return Message
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Message
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param string $sender
     * @return Message
     */
    public function setSender($sender)
    {
        $this->sender = $sender;
        return $this;
    }

    /**
     * @return string
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * @param string $senderName
     * @return Message
     */
    public function setSenderName($senderName)
    {
        $this->senderName = $senderName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSenderNumber()
    {
        return $this->senderNumber;
    }

    /**
     * @param string $senderNumber
     * @return Message
     */
    public function setSenderNumber($senderNumber)
    {
        $this->senderNumber = $senderNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getSenderCountry()
    {
        return $this->senderCountry;
    }

    /**
     * @param string $senderCountry
     * @return Message
     */
    public function setSenderCountry($senderCountry)
    {
        $this->senderCountry = $senderCountry;
        return $this;
    }

    /**
     * @return string
     */
    public function getRawText()
    {
        return $this->rawText;
    }

    /**
     * @param string $rawText
     * @return Message
     */
    public function setRawText($rawText)
    {
        $this->rawText = $rawText;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Message
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getRawPhoneNumber()
    {
        return $this->rawPhoneNumber;
    }

    /**
     * @param string $rawPhoneNumber
     * @return Message
     */
    public function setRawPhoneNumber($rawPhoneNumber)
    {
        $this->rawPhoneNumber = $rawPhoneNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getRawSenderNumber()
    {
        return $this->rawSenderNumber;
    }

    /**
     * @param string $rawSenderNumber
     * @return Message
     */
    public function setRawSenderNumber($rawSenderNumber)
    {
        $this->rawSenderNumber = $rawSenderNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    /**
     * @param string $orderStatus
     * @return Message
     */
    public function setOrderStatus($orderStatus)
    {
        $this->orderStatus = $orderStatus;
        return $this;
    }

    /**
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param string $product
     * @return Message
     */
    public function setProduct($product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @param string $rawText
     * @return string
     */
    public function parse($rawText = '')
    {
        $this->rawText = $rawText;
        $text = $rawText;

        $text = str_replace("#:fname:", $this->firstName, $text);
        $text = str_replace("#:lname:", $this->lastName, $text);
        if($this->orderNumber) {
            $text = str_replace("#:onmr:", $this->orderNumber, $text);
        }
        if ($this->tntCode) {
            $text = str_replace("#:tnt:", $this->tntCode, $text);
        }
        $text = str_replace("#:cname:", $this->senderName, $text);
        $text = str_replace("#:cmail:", $this->rawSenderNumber, $text);
        if ($this->product) {
            $text = str_replace("#:artc:", $this->product, $text);
        }

        $this->message = $text;
        return $text;
    }
}
