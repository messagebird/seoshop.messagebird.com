<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Uuid;

/**
 * SeoshopAction
 *
 * @ORM\Table(name="seoshop_action")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class SeoshopAction extends TimestampableEntity
{
    const ORDER_CREATED = 1;
    const ORDER_PAID = 2;
    const ORDER_SHIPPED = 3;
    const ORDER_UPDATED = 4;
    const PRODUCT_UPDATED = 5;
    const ORDER_SHIPPED_NOTNT = 6;

    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var SeoshopShop
     *
     * @ORM\ManyToOne(targetEntity="SeoshopShop")
     * @ORM\JoinColumn(name="seoshop_shop", referencedColumnName="id")
     */
    private $seoshopShop;

    /**
     * @var SeoshopHook
     *
     * @ORM\ManyToOne(targetEntity="SeoshopHook")
     * @ORM\JoinColumn(name="seoshop_hook", referencedColumnName="id")
     */
    private $seoshopHook;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $action;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $toOwner = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $toClient = 0;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $message;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return SeoshopShop
     */
    public function getSeoshopShop()
    {
        return $this->seoshopShop;
    }

    /**
     * @param SeoshopShop $seoshopShop
     * @return SeoshopAction
     */
    public function setSeoshopShop($seoshopShop)
    {
        $this->seoshopShop = $seoshopShop;
        return $this;
    }

    /**
     * @return SeoshopHook
     */
    public function getSeoshopHook()
    {
        return $this->seoshopHook;
    }

    /**
     * @param SeoshopHook $seoshopHook
     * @return SeoshopAction
     */
    public function setSeoshopHook($seoshopHook)
    {
        $this->seoshopHook = $seoshopHook;
        return $this;
    }

    /**
     * @return int
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param int $action
     * @return SeoshopAction
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isToOwner()
    {
        return $this->toOwner;
    }

    /**
     * @param boolean $toOwner
     * @return SeoshopAction
     */
    public function setToOwner($toOwner)
    {
        $this->toOwner = $toOwner;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isToClient()
    {
        return $this->toClient;
    }

    /**
     * @param boolean $toClient
     * @return SeoshopAction
     */
    public function setToClient($toClient)
    {
        $this->toClient = $toClient;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return SeoshopAction
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }
}
