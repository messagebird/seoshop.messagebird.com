<?php
namespace AppBundle\Entity;

use AppBundle\Model\SeoOrder;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Notive\SEOshopBundle\Model\WebshopInterface;

/**
 * SeoshopShop
 *
 * @ORM\Table(name="seoshop_shop")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity()
 */
class SeoshopShop extends TimestampableEntity implements WebshopInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="seoshop_id", type="string", length=36)
     */
    private $seoshopId;

    /**
     * @var string
     *
     * @ORM\Column(name="seoshop_api_key", type="string", length=255)
     */
    private $seoshopApiKey;

    /**
     * @var string
     *
     * @ORM\Column(name="seoshop_api_token", type="string", length=255)
     */
    private $seoshopApiToken;

    /**
     * @var string
     *
     * @ORM\Column(name="seoshop_language", type="string", length=255)
     */
    private $seoshopLanguage;

    /**
     * @var string
     *
     * @ORM\Column(name="seoshop_name", type="string", length=255, nullable=true)
     */
    private $seoshopName;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", options={"default":1})
     */
    private $enabled;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return SeoshopShop
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getSeoshopId()
    {
        return $this->seoshopId;
    }

    /**
     * @param string $seoshopId
     * @return SeoshopShop
     */
    public function setSeoshopId($seoshopId)
    {
        $this->seoshopId = $seoshopId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSeoshopApiKey()
    {
        return $this->seoshopApiKey;
    }

    /**
     * @param string $seoshopApiKey
     * @return SeoshopShop
     */
    public function setSeoshopApiKey($seoshopApiKey)
    {
        $this->seoshopApiKey = $seoshopApiKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getSeoshopApiToken()
    {
        return $this->seoshopApiToken;
    }

    /**
     * @param string $seoshopApiToken
     * @return SeoshopShop
     */
    public function setSeoshopApiToken($seoshopApiToken)
    {
        $this->seoshopApiToken = $seoshopApiToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getSeoshopLanguage()
    {
        return $this->seoshopLanguage;
    }

    /**
     * @param string $seoshopLanguage
     * @return SeoshopShop
     */
    public function setSeoshopLanguage($seoshopLanguage)
    {
        $this->seoshopLanguage = $seoshopLanguage;
        return $this;
    }

    /**
     * @return string
     */
    public function getSeoshopName()
    {
        return $this->seoshopName;
    }

    /**
     * @param string $seoshopName
     * @return SeoshopShop
     */
    public function setSeoshopName($seoshopName)
    {
        $this->seoshopName = $seoshopName;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     * @return SeoshopShop
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * True if the SEOshop is active
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $isActive
     * @return SeoshopShop
     */
    public function setIsActive($isActive)
    {
       $this->enabled = $isActive;
    }


    public function createApiClient()
    {
        return new \WebshopappApiClient(
            'live',
            $this->getSeoshopApiKey(),
            $this->getSeoshopApiToken(),
            $this->getSeoshopLanguage()
        );
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }
}
