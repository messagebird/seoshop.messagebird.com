<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MessagebirdAccount
 *
 * @ORM\Table(name="messagebird_account")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class MessagebirdAccount extends TimestampableEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @var SeoshopShop
     *
     * @ORM\OneToOne(targetEntity="SeoshopShop")
     * @ORM\JoinColumn(name="seoshop_shop", referencedColumnName="id")
     */
    protected $seoshopShop;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $apiKey;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $sender;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $contactName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $contactNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $country;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return SeoshopShop
     */
    public function getSeoshopShop()
    {
        return $this->seoshopShop;
    }

    /**
     * @param SeoshopShop $seoshopShop
     * @return MessagebirdAccount
     */
    public function setSeoshopShop($seoshopShop)
    {
        $this->seoshopShop = $seoshopShop;
        return $this;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     * @return MessagebirdAccount
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param string $sender
     * @return MessagebirdAccount
     */
    public function setSender($sender)
    {
        $sender = preg_replace("/[^A-Za-z0-9 ]/", "", $sender);
        $this->sender = $sender;
        return $this;
    }

    /**
     * @return string
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * @param string $contactName
     * @return MessagebirdAccount
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;
        return $this;
    }

    /**
     * @return string
     */
    public function getContactNumber()
    {
        return $this->contactNumber;
    }

    /**
     * @param string $contactNumber
     * @return MessagebirdAccount
     */
    public function setContactNumber($contactNumber)
    {
        $this->contactNumber = $contactNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return MessagebirdAccount
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }
}

