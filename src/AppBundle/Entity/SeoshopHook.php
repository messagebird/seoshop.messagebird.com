<?php
namespace AppBundle\Entity;

use ClassesWithParents\D;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * SeoshopHook
 *
 * @ORM\Table(name="seoshop_hook")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class SeoshopHook extends TimestampableEntity
{
    const HOOK_ORDER_CREATED = 1;
    const HOOK_ORDER_PAID = 2;
    const HOOK_ORDER_SHIPPED = 3;
    const HOOK_ORDER_UPDATED = 4;
    const HOOK_PRODUCT_UPDATED = 5;

    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var SeoshopShop
     *
     * @ORM\ManyToOne(targetEntity="SeoshopShop")
     * @ORM\JoinColumn(name="seoshop_shop", referencedColumnName="id")
     * */
    private $seoshopShop;

    /**
     * @var string
     *
     * @ORM\Column(name="hook_id", type="string", length=45, nullable=true)
     */
    private $hookId;

    /**
     * @var string
     *
     * @ORM\Column(name="item_group", type="string", length=255)
     */
    private $itemGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="item_action", type="string", length=255)
     */
    private $itemAction;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", options={"default" = 0})
     */
    private $enabled;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return SeoshopShop
     */
    public function getSeoshopShop()
    {
        return $this->seoshopShop;
    }

    /**
     * @param SeoshopShop $seoshopShop
     * @return SeoshopHook
     */
    public function setSeoshopShop($seoshopShop)
    {
        $this->seoshopShop = $seoshopShop;
        return $this;
    }

    /**
     * @return string
     */
    public function getHookId()
    {
        return $this->hookId;
    }

    /**
     * @param string $hookId
     * @return SeoshopHook
     */
    public function setHookId($hookId)
    {
        $this->hookId = $hookId;
        return $this;
    }

    /**
     * @return string
     */
    public function getItemGroup()
    {
        return $this->itemGroup;
    }

    /**
     * @param string $itemGroup
     * @return SeoshopHook
     */
    public function setItemGroup($itemGroup)
    {
        $this->itemGroup = $itemGroup;
        return $this;
    }

    /**
     * @return string
     */
    public function getItemAction()
    {
        return $this->itemAction;
    }

    /**
     * @param string $itemAction
     * @return SeoshopHook
     */
    public function setItemAction($itemAction)
    {
        $this->itemAction = $itemAction;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     * @return SeoshopHook
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @var int $index
     * @return array
     */
    public static function getDefaultActions($index = null)
    {
        if($index > 5){
            return false;
        }
        $holder = array(
            self::HOOK_ORDER_CREATED => array(
                'id' => 1,
                'name' => 'Order created',
                'label' => 'order_created',
                'itemGroup' => 'orders',
                'itemAction' => 'created',
            ),
            self::HOOK_ORDER_PAID => array(
                'id' => 2,
                'name' => 'Order paid',
                'label' => 'order_paid',
                'itemGroup' => 'orders',
                'itemAction' => 'paid',
            ),
            self::HOOK_ORDER_SHIPPED => array(
                'id' => 3,
                'name' => 'Order shipped',
                'label' => 'order_shipped',
                'itemGroup' => 'orders',
                'itemAction' => 'shipped',
            ),
            self::HOOK_ORDER_UPDATED => array(
                'id' => 4,
                'name' => 'Order updated',
                'label' => 'order_updated',
                'itemGroup' => 'orders',
                'itemAction' => 'updated',
            ),
            self::HOOK_PRODUCT_UPDATED => array(
                'id' => 5,
                'name' => 'Product updated',
                'label' => 'product_updated',
                'itemGroup' => 'products',
                'itemAction' => 'updated',
            ),
        );
        if($index) {
            return $holder[$index];
        }
        return $holder;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }
}
