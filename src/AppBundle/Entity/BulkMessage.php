<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BulkMessage
 *
 * @ORM\Table(name="bulk_messages")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class BulkMessage extends TimestampableEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var SeoshopShop
     *
     * @ORM\ManyToOne(targetEntity="SeoshopShop")
     * @ORM\JoinColumn(name="seoshop_shop", referencedColumnName="id")
     */
    private $seoshopShop;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="order_from_date", type="datetime")
     */
    private $orderFromDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="order_to_date", type="datetime")
     */
    private $orderToDate;

    /**
     * @var string
     *
     * @ORM\Column(name="article_code", type="string", length=255, nullable=true)
     */
    private $articleCode;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return SeoshopShop
     */
    public function getSeoshopShop()
    {
        return $this->seoshopShop;
    }

    /**
     * @param SeoshopShop $seoshopShop
     * @return BulkMessage
     */
    public function setSeoshopShop($seoshopShop)
    {
        $this->seoshopShop = $seoshopShop;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getOrderFromDate()
    {
        return $this->orderFromDate;
    }

    /**
     * @param \DateTime $orderFromDate
     * @return BulkMessage
     */
    public function setOrderFromDate($orderFromDate)
    {
        $this->orderFromDate = $orderFromDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getOrderToDate()
    {
        return $this->orderToDate;
    }

    /**
     * @param \DateTime $orderToDate
     * @return BulkMessage
     */
    public function setOrderToDate($orderToDate)
    {
        $this->orderToDate = $orderToDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getArticleCode()
    {
        return $this->articleCode;
    }

    /**
     * @param string $articleCode
     * @return BulkMessage
     */
    public function setArticleCode($articleCode)
    {
        $this->articleCode = $articleCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return BulkMessage
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }
}
