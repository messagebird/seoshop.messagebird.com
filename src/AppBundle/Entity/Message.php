<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="messages")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Message extends TimestampableEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var SeoshopAction
     *
     * @ORM\ManyToOne(targetEntity="SeoshopAction")
     * @ORM\JoinColumn(name="seoshop_action", referencedColumnName="id", nullable = true)
     */
    private $shopAction;


    /**
     * @var BulkMessage
     *
     * @ORM\ManyToOne(targetEntity="BulkMessage")
     * @ORM\JoinColumn(name="bulk_message", referencedColumnName="id", nullable = true)
     */
    private $bulkMessage;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $clientData;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=30)
     */
    private $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="text", length=180)
     */
    private $message;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return SeoshopAction
     */
    public function getShopAction()
    {
        return $this->shopAction;
    }

    /**
     * @param SeoshopAction $shopAction
     * @return Message
     */
    public function setShopAction($shopAction)
    {
        $this->shopAction = $shopAction;
        return $this;
    }

    /**
     * @return BulkMessage
     */
    public function getBulkMessage()
    {
        return $this->bulkMessage;
    }

    /**
     * @param BulkMessage $bulkMessage
     * @return Message
     */
    public function setBulkMessage($bulkMessage)
    {
        $this->bulkMessage = $bulkMessage;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientData()
    {
        return $this->clientData;
    }

    /**
     * @param string $clientData
     * @return Message
     */
    public function setClientData($clientData)
    {
        $this->clientData = $clientData;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     * @return Message
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Message
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }
}
