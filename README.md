# Messagebird SEOshop instructions

## SEOshop APP

### Requirements:
- PHP 7.0
- SSL
- MySQL

### Deployment with Deis

#### First time

1. First login at deis `deis login http://deis.104.199.28.204.nip.io` Username is admin, password is on 1Password
2. Add your key `deis keys:add ~/.ssh/id_deis.pub`
3. Configure env on deis `deis config:set SYMFONY_ENV=prod`
4. Add the SYMFONY_DATABASE_URL env var with `deis config:set "SYMFONY__DATABASE__URL=mysql://seoshop_staging:(sql_pass)@104.199.24.226/seoshop_staging" -a (app_name)`
5. Add git remote with `git remote add staging ssh://git@deis-builder.104.199.28.204.nip.io:2222/seoshop-staging.git`
6. Add env var `deis config:set SYMFONY__SEOSHOP__APISECRET=(app_apisecret_key)`
7. Add env var `deis config:set SYMFONY__SEOSHOP__APIKEY=(app_apikey)`

#### Deploy it directly with

1. Login at deis `deis login http://deis.104.199.28.204.nip.io`
2. Deploy with `git push staging (local-git-branch):master`


### Installation instructions

1. Check the enironment: `php app/check/php`
2. Export the production environment: `export SYMFONY_ENV=prod`
3. Use composer.phar to download required libraries `php composer.phar install --no-dev --optimize-autoloader`  
4. Follow instructions from the composer command to set up mail and database settings. Use http://nux.net/secret to generate a new secret token.
5. Create the database schema: `php app/console doctrine:schema:create`
6. In `app/config.yml` change the seoshop parameters at the bottom of the file to match your seoshop app, do not change `shop_class` or `success_route`.
7. Clear the cache: `php app/console cache:clear --env=prod --no-debug`

### Update instructions

1. Export the production environment: `export SYMFONY_ENV=prod`
2. Use composer.phar to update libraries `php composer.phar update --no-dev --optimize-autoloader`
3. Update the database schema `php app/console doctrine:schema:update`

## SEOshop API

### Setup instructions

1. Log in to your lightspeed partner account: https://seoshop.webshopapp.com/partners
2. Go to your app
3. Set success url to `[app_url]/seoshop/install` replacing `[app_url]` with your app url
4. Set cancel url to `[app_url]/seoshop/cancelled`
5. Set unistall url to `[app_url]/seoshop/uninstall`
6. Set app login url to `https://www.messagebird.com/`
7. Set permissions to ALL read permissions
8. Set up your icon and screenshots
9. Copy the api key and api secret into `app/config.yml` see SEOshop APP installation instructions.
